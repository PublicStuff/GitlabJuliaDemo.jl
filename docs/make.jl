using Documenter, GitlabJuliaDemo

makedocs(
    modules = [GitlabJuliaDemo],
    format = :html,
    checkdocs = :exports,
    sitename = "GitlabJuliaDemo.jl",
    pages = Any["index.md"],
    repo = "https://gitlab.com/tkpapp/GitlabJuliaDemo.jl/blob/{commit}{path}#{line}"
)
